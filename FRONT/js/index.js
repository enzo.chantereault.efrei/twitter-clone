const API_URL = "http://localhost:3000/tweets";
let nextPageUrl = null;

const onEnter = (event) => {
    if (event.key != "Enter")
        return;
    getTwitterData();
}

const selectTrend = (event) => {
    const text = event.innerText;
    document.getElementById("search-input").value = text;
    getTwitterData();
}

const onNextPage = () => {
    if (!nextPageUrl)
        return;

    getTwitterData(true);
}

const getTwitterData = (nextPage = null) => {
    const query = document.getElementById("search-input").value
    if (!query)
        return;

    const encodedQuery = encodeURIComponent(query);
    const count = 10;
    let fullUrl = "";

    if (nextPage) {
        fullUrl = nextPageUrl;
    } else {
        fullUrl = `${API_URL}?q=${encodedQuery}&count=${count}`;
    }

    fetch(fullUrl).then(
        res => {
            console.log(res);
            return res.json();
        }
    ).then(
        data => {
            buildTweets(data.statuses, nextPage);
            saveNextPage(data.search_metadata);
        }
    );
}

const saveNextPage = (metadata) => {
    if (metadata.next_results) {
        nextPageUrl = `${API_URL}${metadata.next_results}`;
        nextPageButtonVisibility(true);
    } else {
        nextPageUrl = null;
        nextPageButtonVisibility(false);
    }
}

const nextPageButtonVisibility = (isVisible = null) => {
    document.querySelector('.next-page-container ').style.visibility = isVisible ? "visible" : "hidden";
}

const buildTweets = (tweets, nextPage = null) => {
    let twitterContent = "";
    tweets.map(
        tweet => {
            const createDate = moment(tweet.created_at).fromNow();
            twitterContent += `                    
                <div class="tweet-container">
                    <div class="tweet-user-info-container">
                        <div class="tweet-user-profile" style="background-image: url(${tweet.user.profile_image_url_https})"></div>
                        <div class="tweet-user-name-container">
                            <div class="tweet-user-fullname">
                                ${tweet.user.name}
                            </div>
                            <div class="tweet-user-username">
                                @${tweet.user.screen_name}
                            </div>
                        </div>
                    </div>
                `;
            if (tweet.extended_entities?.media.length > 0) {
                twitterContent += `
                    <div class="tweet-media-container">
                        ${buildImages(tweet.extended_entities.media)}
                        ${buildVideo(tweet.extended_entities.media)}
                    </div>`;
            }
            twitterContent += `
                    <div class="tweet-text-container">
                        ${tweet.full_text}
                    </div>
                    <div class="tweet-date-container">
                        ${createDate}
                    </div>
                </div>
            `;
        }
    );

    if (nextPage) {
        document.querySelector('.tweets-list-container').insertAdjacentHTML("beforeend", twitterContent);
    } else {
        document.querySelector('.tweets-list-container').innerHTML = twitterContent;
    }
}

const buildImages = (mediaList) => {
    let imagesContent = '';
    let ismediaTypeImage = false;

    mediaList.map(
        media => {
            if (media.type == "photo") {
                ismediaTypeImage = true;
                imagesContent += `
                    <div class="tweet-image" style="background-image: url(${media.media_url_https})">
                    </div>
                `;
            }
        }
    );
    return ismediaTypeImage ? imagesContent : '';
}

const buildVideo = (mediaList) => {
    let videoContent = '';
    let ismediaTypeVideo = false;

    mediaList.map(
        media => {
            if (media.type == "video") {
                ismediaTypeVideo = true;
                const videoVariant = media.video_info.variants.find(variant => variant.content_type == 'video/mp4');
                videoContent += `
                    <video class="tweet-video" controls>
                        <source src="${videoVariant.url}" type="video/mp4"></source>
                    </video>
                `;
            } else if (media.type == "animated_gif") {
                ismediaTypeVideo = true;
                videoContent += `
                    <video class="tweet-video" loop autoplay>
                        <source src="${media.video_info.variants[0].url}" type="video/mp4"></source>
                    </video>
                `;
            }
        }
    );

    return ismediaTypeVideo ? videoContent : '';
}
