const axios = require('axios');
const TWITTER_API_URL = 'https://api.twitter.com/1.1/search/tweets.json';

class Twitter {

    getTweets(query, maxId = null, count) {
        return axios.get(TWITTER_API_URL, {
            params: {
                q: query,
                max_id: maxId,
                count: count,
                tweet_mode: "extended"
            },
            headers: {
                'Authorization': `Bearer ${process.env.TWITTER_API_TOKEN}`
            }
        });
    }
}

module.exports = Twitter;