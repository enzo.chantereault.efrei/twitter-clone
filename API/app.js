require('dotenv').config();
const express = require('express');
const app = express();
const port = 3000;
const Twitter = require('./helpers/twitter.helper.js');

app.use(
    (req, res, next) => {
        res.header('Access-Control-Allow-Origin', '*');
        next();
    }
);

app.get('/tweets', (req, res) => {
    const twitter = new Twitter();
    const query = req.query.q;
    const count = req.query.count;
    const maxId = req.query.max_id;

    twitter.getTweets(query, maxId, count).then(
        response => {
            res.status(200).send(response.data);
        }
    ).catch(
        error => {
            res.status(400).send(error);
        }
    );

});

app.listen(port, () => {
    console.log(`Twitter API is listening at port ${port}`)
});